#include <vector>
#include <algorithm>
#include <iostream>

struct Find
{
    Find()
        :
        max(0),
        start(0),
        end(0),
        tempMax(0)
    { }

    void operator()(int value);

    int max;
    int start;
    int end;
    int tempMax;
};

void Find::operator()(int value)
{
    static auto temp = 0;
    static auto newStart = false;
    static auto idx = -1;

    ++idx;

    /* for negative max */
    if (max < 0)
    {
        if (max < value)
        {
            max = value, start = idx, end = idx;
            tempMax = max;
        }

        return;
    }

    temp = tempMax + value;
    if (temp < 0)
    {
        newStart = true;
        tempMax = 0;

        return;
    }

    tempMax = temp;

    if (tempMax >= max)
    {
        max = tempMax;
        end = idx;
        if (newStart)
        {
            start = idx;
            newStart = false;
        }
    }

    return;
}


int main()
{
    std::vector<int> array{ 1, 2, -5, 3, 2, -1, 5, -10, 3, 2 };

    if (array.empty())
    {
        std::cout << "Array size is 0" << std::endl;
        return 1;
    }

    Find result = std::for_each(array.begin(), array.end(), Find());

    std::cout << "Max is " << result.max << ". Start at " << result.start << ". End at " << result.end << std::endl;

    return 0;

};
